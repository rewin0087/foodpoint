PLACES_API = Her::API.new
PLACES_API.setup url: ENV['HERE_MAP_PLACES_API'] do |c|
  # Response
  c.use Her::Middleware::DefaultParseJSON
  c.use Faraday::Response::Logger, logger = Rails.logger
  # Adapter
  c.use Faraday::Adapter::NetHttp
end

GOOGLE_MAP_API = Her::API.new
GOOGLE_MAP_API.setup url: ENV['GOOGLE_MAP_API'] do |c|
  # Response
  c.use Her::Middleware::DefaultParseJSON
  c.use Faraday::Response::Logger, logger = Rails.logger
  # Adapter
  c.use Faraday::Adapter::NetHttp
end
