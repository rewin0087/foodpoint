Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :users, only: [:create]
      resources :places, only: [:index]
    end
  end

  get 'sign_in', to: 'authentication#sign_in'
  get 'sign_out', to: 'authentication#sign_out'

  resources :locations, only: [:index, :show]

  root 'home#index'
end