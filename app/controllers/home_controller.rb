class HomeController < ApplicationController
  before_filter :current_location_present?
  def index
    # @nearest_restaurants = Maps::Api::Place.nearby_food_places("#{session[:current_latitude]},#{session[:current_longitude]}")
    @nearest_restaurants = Here::Place.search({ latitude: session[:current_latitude], longitude: session[:current_longitude], keyword: 'restaurant' })
    gon.nearest_restaurants = @nearest_restaurants.attributes
  end

  private
    def current_location_present?
      flash.now[:error] = 'Refreshing Location...' if session[:current_latitude].blank? && session[:current_longitude].blank?
    end
end
