class AuthenticationController < ApplicationController
  skip_before_filter :authenticate_user!
  layout 'dialog'

  def sign_in
    oauth = Koala::Facebook::OAuth.new(ENV['FACEBOOK_APP_ID'], ENV['FACEBOOK_APP_SECRET'], ENV['FACEBOOK_CALLBACK_URL'])
    @fb_url = oauth.url_for_oauth_code({ permissions: ENV['FACEBOOK_PERMISSIONS'] })

    if code = params[:code]
      access_token = oauth.get_access_token(code)
      store_facebook_access_token(access_token)
      retreive_current_user_data!
      return redirect_to root_path, turbolinks: true
    end

    redirect_to root_path, turbolinks: true if facebook_access_token
  end

  def sign_out
    reset_session
    redirect_to sign_in_path, turbolinks: true
  end

  private
    def store_facebook_access_token(access_token)
      reset_session
      session[:facebook_access_token] = access_token
      session[:sign_in_time] = Time.zone.now
    end

    def retreive_current_user_data!
      session[:current_user] = facebook.get_object('me', {}, api_version: ENV['FACEBOOK_API_VERSION'])
      session[:profile_small_picture] = facebook.get_picture(session[:current_user]['id'], { type: 'small'})
      session[:profile_big_picture] = facebook.get_picture(session[:current_user]['id'], { type: 'normal'})
    end
end
