class Api::V1::UsersController < ApplicationController
  skip_before_action :load_current_user_data!
  skip_before_action :send_variables_to_js
  
  def create
    if params[:latitude] && params[:longitude]
      store_current_location(params[:latitude], params[:longitude])
    end

    render text: nil
  end

  def show
  end

  private
    def store_current_location(latitude, longitude)
      session[:current_latitude] = latitude
      session[:current_longitude] = longitude
    end
end
