class LocationsController < ApplicationController

  def index
    return search_restaurants(params[:q]) if params[:q]
  end

  def show
    @location_detail = params
    gon.location_detail = @location_detail
  end

  private

    def search_restaurants(keyword)
      parameters = { latitude: '14.5576' || session[:current_latitude], longitude: '121.02' || session[:current_longitude], keyword: keyword }
      @results = Here::Place.search_with_wrap_to_model(parameters)
      gon.search_results = @results
      render
    end
end
