class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  rescue_from Faraday::ConnectionFailed, with: :api_inaccessible 
  before_filter :authenticate_user!
  before_action :load_current_user_data!
  before_action :send_variables_to_js
  helper_method :sign_in_time
  
  protected
    def send_variables_to_js
      gon.clear
      gon.here = { app_id: ENV['HERE_MAP_APP_ID'], app_code: ENV['HERE_MAP_APP_CODE'] }
      gon.visitor_location = visitor_location
      gon.geo_session = { lat: session[:current_latitude], lng: session[:current_longitude] }
      gon.user_picture = @profile_small_picture
    end

    def facebook
      @facebook ||= Koala::Facebook::API.new(facebook_access_token)
    end

    def facebook_access_token
      session[:facebook_access_token]
    end

    def visitor_location
      @visitor_location ||= request.location
    end

    def api_inaccessible
      flash.now[:error] = 'Connection Lost, Please Try Again.'
      render
    end

    def sign_in_time
      DateTime.strptime(session[:sign_in_time], "%Y-%m-%d %H:%M:%S")
    end

    def load_current_user_data!
      unless session[:facebook_access_token].blank?
        @current_user ||= session[:current_user]
        @profile_small_picture ||= session[:profile_small_picture]
        @profile_big_picture ||= session[:profile_big_picture]
      end
    end

    def authenticate_user!
      redirect_to sign_in_path, turbolinks: true if session[:facebook_access_token].blank?
    end
end
