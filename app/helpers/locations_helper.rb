module LocationsHelper
  def category_color_theme(category)
    categories = Here::Place::FOOD_CATEGORY

    if category == categories[0]
      'bg-red'
    elsif category == categories[1]
      'bg-aqua'
    elsif category == categories[2]
      'bg-orange'
    elsif category == categories[3]
      'bg-green'
    else
      'bg-teal'
    end
  end

  def active?(location_detail, mode)
    'active' if location_detail[:active_tab] == mode
  end

  def location_path_options(place, results)
    location = @results[:search][:context][:location]
    {
      coords: "#{place.position.first},#{place.position.second}",
      title: place.title, 
      vicinity: place.vicinity, 
      current_location: location[:address][:text],
      current_coords: "#{location[:position].first},#{location[:position].second}",
      current_lat: location[:position].first,
      current_long: location[:position].second,
      active_tab: 'public_transport_route'
    }
  end
end
