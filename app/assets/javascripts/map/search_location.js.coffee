class window.SearchLocation
  map: null
  bubble: null
  ui: null
  group: null
  geocoder: null
  results: []

  constructor: () ->
    @group = new H.map.Group()
    @geocoder = platform.getGeocodingService()

  search: (keyword) =>
    geocodingParameters = { searchText: keyword }
    @geocoder.geocode(
      geocodingParameters,
      @onSuccess,
      @onError
    )

  onSuccess: (result) =>
    @locations = result.Response.View[0].Result

  onError: (error) =>
    console.log("Error" + error)