addInfoBubble = (items, hereMap) ->
  group = new H.map.Group()
  map.addObject(group)

  group.addEventListener 'tap', (evt) =>
    hereMap.openBubble(evt.target.getPosition(), evt.target.getData())
  , false

  items.map (e) =>
    if !!e
      addMarkerToGroup(group, { lat: e.position[0], lng: e.position[1] },
        "<div class='title'>" + e.title +
        "</div><div class='vicinity' >" + e.vicinity +
        "</div>")

addMarkerToGroup = (group, coordinate, html) ->
  marker = new H.map.Marker(coordinate)
  marker.setData(html)
  group.addObject(marker)


$(document).on 'page:load page:ready ready', () ->
  if $('body').hasClass('home')
    defaultLayers = platform.createDefaultLayers()
    if(!!gon.geoSession.lat && !!gon.geoSession.lng)
      window.map = new H.Map(document.getElementById('map'),
      	defaultLayers.normal.map, {
      		center:
      			lat: gon.geoSession.lat,
      			lng: gon.geoSession.lng
      		,
      		zoom: 18
      	}
      )

      window.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map))

      window.ui = H.ui.UI.createDefault(map, defaultLayers)

      hereMap = new HereMap()
      hereMap.map = map
      hereMap.ui = ui
      hereMap.plotCurrentLocation(gon.geoSession.lat + "," + gon.geoSession.lng)

    	addInfoBubble(gon.nearestRestaurants.results.items, hereMap)
