

mapWithPublicTransportRoute = (platform, mode) ->
  # load routes
  defaultLayers = platform.createDefaultLayers()
  map = new H.Map(document.getElementById('map_with_public_transport_route'),
    defaultLayers.normal.map,
    center: 
      lat: gon.locationDetail.current_lat 
      lng: gon.locationDetail.current_long
    zoom: 13
  )

  behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map))
  ui = H.ui.UI.createDefault(map, defaultLayers)

  router = platform.getRoutingService()
  routeRequestParams = {
    mode: mode,
    representation: 'display',
    waypoint0: gon.locationDetail.current_coords,
    waypoint1: gon.locationDetail.coords
  }

  hereMap = new HereMap()
  hereMap.map = map
  hereMap.ui = ui
  hereMap.plotCurrentLocation(gon.locationDetail.current_coords)
  hereMap.plotDistination(gon.locationDetail.coords)

  router.calculateRoute(routeRequestParams, hereMap.onSuccess, hereMap.onError)

mapWithDrivingroute = (platform, mode) ->
  # load routes
  defaultLayers = platform.createDefaultLayers()
  map = new H.Map(document.getElementById('map_with_driving_route'),
    defaultLayers.normal.map,
    center: 
      lat: gon.locationDetail.current_lat 
      lng: gon.locationDetail.current_long
    zoom: 13
  )

  behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map))
  ui = H.ui.UI.createDefault(map, defaultLayers)

  router = platform.getRoutingService()
  routeRequestParams = {
    mode: mode,
    representation: 'display',
    waypoint0: gon.locationDetail.current_coords,
    waypoint1: gon.locationDetail.coords
  }

  hereMap = new HereMap()
  hereMap.map = map
  hereMap.ui = ui
  hereMap.plotCurrentLocation(gon.locationDetail.current_coords)
  hereMap.plotDistination(gon.locationDetail.coords)

  router.calculateRoute(routeRequestParams, hereMap.onSuccess, hereMap.onError)

mapWithPedestrianRoute = (platform, mode) ->
  # load routes
  defaultLayers = platform.createDefaultLayers()
  map = new H.Map(document.getElementById('map_with_pedestrian_route'),
    defaultLayers.normal.map,
    center: 
      lat: gon.locationDetail.current_lat 
      lng: gon.locationDetail.current_long
    zoom: 13
  )

  behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map))
  ui = H.ui.UI.createDefault(map, defaultLayers)

  router = platform.getRoutingService()
  routeRequestParams = {
    mode: mode,
    representation: 'display',
    waypoint0: gon.locationDetail.current_coords,
    waypoint1: gon.locationDetail.coords
  }

  hereMap = new HereMap()
  hereMap.map = map
  hereMap.ui = ui
  hereMap.plotCurrentLocation(gon.locationDetail.current_coords)
  hereMap.plotDistination(gon.locationDetail.coords)
  
  router.calculateRoute(routeRequestParams, hereMap.onSuccess, hereMap.onError)

$(document).on 'page:load page:ready ready', () ->
  if $('div').hasClass('maps')
    mapWithPublicTransportRoute(platform, 'fastest;publicTransport')
    mapWithDrivingroute(platform, 'fastest;car')
    mapWithPedestrianRoute(platform, 'shortest;pedestrian')

    $('#map-tabs a').on 'click', (e) =>
      e.preventDefault()
      dis = $(e.currentTarget)
      
      url = window.location.href.split('?')

      query = url[1].split('&')
      domain = "#{url[0]}?"
      active_tabs = ['active_tab=public_transport_route', 'active_tab=driving_route', 'active_tab=pedestrian_route']
      new_query = []
      $.each query, (i, param) ->
        new_query.push(param) unless active_tabs.indexOf(param) != -1
      new_url = "#{domain}#{new_query.join('&')}"
      Turbolinks.visit(new_url + '&active_tab=' + dis.attr('data-tab'))

