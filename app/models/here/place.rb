module Here
  class Place
    include Her::Model
    use_api PLACES_API
    collection_path 'places/v1/discover/search?'

    FOOD_CATEGORY = %w(restaurant snacks-fast-food coffee-tea kiosk-convenience-store)
    
    def self.search(params)
      query = { 
        at: "#{params[:latitude]},#{params[:longitude]}",
        q: params[:keyword],
        accept: 'application/json',
        app_id: ENV['HERE_MAP_APP_ID'],
        app_code: ENV['HERE_MAP_APP_CODE']
      }.to_param

      self.get("#{collection_path}#{query}")
    end

    def self.search_with_wrap_to_model(params)
      response = self.search(params)
      collection = response.results[:items].map {|item| FOOD_CATEGORY.include?(item[:category][:id]) ? self.new(item) : nil }.compact
      search = response.search

      { collection: collection, search: search }
    end
  end
end