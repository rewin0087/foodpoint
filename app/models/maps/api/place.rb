module Maps
  class Api::Place
    include Her::Model
    use_api GOOGLE_MAP_API
    collection_path 'maps/api/place'
    RADIUS = 800
    FOOD = 'food'

    def self.search_nearby_food_places(params)
      self.get("#{collection_path}/nearbysearch/json", params)
    end

    def self.nearby_food_places(location)
      params = {
        location: location,
        radius: RADIUS,
        type: FOOD,
        key: ENV['GOOGLE_API_KEY']
      }

      response = self.search_nearby_food_places(params)
      response.results
    end
  end
end